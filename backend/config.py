"""
    This is a configuration file, holding global variables necessary for the functionality
    of the "MyChat" server.
"""

# Where the BE server resides.
server_address = "10.0.0.14"
server_port = 3200
# Where the FE is served from.
client_address = "10.0.0.14"

# paths to the ssl keys:
ssl_keyfile_path = "../setup.key"
ssl_certificate_path = "../setup.crt"

# path to a json file used as database
json_db_path = "./db.json"

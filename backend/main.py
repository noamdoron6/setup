import uvicorn
from config import server_address, server_port, ssl_keyfile_path, ssl_certificate_path

if __name__ == "__main__":
    uvicorn.run("api:app",
                host=server_address,
                port=server_port,
                reload=True,
                ssl_keyfile=ssl_keyfile_path,
                ssl_certfile=ssl_certificate_path)
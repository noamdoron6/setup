from config import json_db_path
import json


class Dal:
    def __init__(self):
        pass

#   MARCO
    @staticmethod
    def answer(call: str):
        with open(json_db_path, 'r+') as db:
            data = json.load(db)
            response = data[call]
        return response

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from dal import Dal

app = FastAPI()

# Served FE origins accessing this BE.
origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.get("/")
async def answer(call: str) -> dict:
    print(call)
    return Dal.answer(call)

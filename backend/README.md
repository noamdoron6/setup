# How to use this server

### Step1 - configure the server:
go to the config file and set the following params to fit your machine:
* server_address
* server_port
* client_address
* ssl_keyfile_path
* ssl_certificate_path

### Step2 - execute the server:
*python main.py*
#!/bin/bash


############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "create an openssl certificate for mychat"
   echo
   echo "Syntax: create-ssl-ky.sh [-p|h] <configuration file path>"
   echo "options:"
   echo "h     Print this Help."
   echo
}

############################################################
############################################################
# Main                                                     #
# The variable "domain" below is what controls the         #
# certificate file names, change it according to the name  #
# of your app or domain.                                   #
############################################################
############################################################

Main() {
	domain=setup
	echo "removing previous key files..."
	rm $domain.key
	rm $domain.crt

	openssl req -x509 -newkey rsa:4096 -sha256 -keyout $domain.key -out $domain.crt -days 365 -config $configurations -new -nodes

	echo "---------------------------"
	echo "-----Below is your CSR-----"
	echo "---------------------------"
	echo
	cat $domain.crt

	echo
	echo "---------------------------"
	echo "-----Below is your Key-----"
	echo "---------------------------"
	echo
	cat $domain.key
}


while getopts ":ph" option; do
   case $option in
   	h) # display Help
   		Help
		exit;;
	\? )
		echo "error - unidentified option."
		exit;;
   esac
done

# if none of the above cases:
configurations=$1
if [ -z "$configurations" ]
then
    echo "Argument not present."
    echo "Useage $0 [configuration file name]"

    exit 99
fi
Main


import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app/App';
import reportWebVitals from './setup/reportWebVitals';
import onSetup from './setup/setup';
import { BrowserRouter } from 'react-router-dom';
import ThemeContextProvider from './context/ThemeContext';

const app = (
  <React.StrictMode>
    <BrowserRouter>
      <ThemeContextProvider>
        <App />
      </ThemeContextProvider>
    </BrowserRouter>
  </React.StrictMode>
);
const element = document.getElementById('root');
const callback = onSetup;

ReactDOM.render(app, element, callback);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

import { BackendButton, GraphButton, HomeButton, ListButton } from '../components/common/buttons';
import { HomePage, ListPage, BackendTestPage } from '../components/pages';
import {IconicRouteType} from '../types';

const RoutesConfig: IconicRouteType[] = [
    {path: '/', destination: HomePage, icon: HomeButton},
    {path: '/list', destination: ListPage, icon: ListButton},
    {path: '/backend', destination: BackendTestPage, icon: BackendButton},
    {path: '/', destination: HomePage, icon: GraphButton},
];

export default RoutesConfig;

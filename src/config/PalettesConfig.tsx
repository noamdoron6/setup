import PaletteType from '../types/PaletteType';


const PalettesConfig = [
    {
        name: 'bright',
        primary: '#FFF3D3',
        secondary: '#DAAD86',
        accent: '#ffffff',
        textPrimary: '#ffffff',
        textSecondary: '#659DBD',
        background: '#659DBD'
    }
] as PaletteType[];

export default PalettesConfig;
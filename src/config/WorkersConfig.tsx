import { WorkerEntryType } from '../types';
import HttpWorker from '../workers/HttpWorker';

const WorkersConfig: WorkerEntryType[] = [
    {name: 'http', worker: HttpWorker}
];

export default WorkersConfig;
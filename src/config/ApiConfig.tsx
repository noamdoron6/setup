import {ApiServiceType} from '../types';

const ApiConfig: ApiServiceType[] = [
    {name: 'backend-server', url: `https://10.0.0.14:3200/`, description: 'small answers service'},
];

export default ApiConfig;
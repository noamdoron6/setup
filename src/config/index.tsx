import RoutesConfig from './RouteConfig';
import WorkersConfig from './WorkersConfig';
import PalettesConfig from './PalettesConfig';
import ApiConfig from './ApiConfig';

/*
* This is an exportation file, it allows easier exportation of objects,
* components, types and globals from this directory.
*/
export {
    ApiConfig,
    RoutesConfig,
    WorkersConfig,
    PalettesConfig
};
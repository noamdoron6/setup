function onSetup(): void {
    setTitle();
}

function setTitle(): void {
    const element = document.getElementById('root-title');
    const title = process.env.REACT_APP_TITLE;
    if(element !== null && title !== undefined) {
        element.textContent = title;
    }
}

export default onSetup;
# Contexts
Here lies the contexts of the app, call them using the 'useContext' hook,\
contexts should contain data that could change the behaviour of the app\
and does not belong to any specific app.
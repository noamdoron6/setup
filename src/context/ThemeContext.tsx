import React, { useState } from 'react';
import {ThemeProvider} from 'styled-components';
import {PalettesConfig} from '../config';

interface ThemeContextState {
    theme: any,
    setTheme: (theme: any) => void
}

export const ThemeContext = React.createContext<ThemeContextState>({
    theme: undefined,
    setTheme: (theme: any) => {}
});

/*
* This context is responsible for the theme colors of the app, as well
* as synching such colors between providers of external dependencies.
*/
const ThemeContextProvider = (props: {children: any}) => {
    const [themeState, setThemeState] = useState(PalettesConfig[0]);

    const onThemeChange = (theme: any) => {
        setThemeState(theme);
    }

    return(<ThemeContext.Provider value={{theme: themeState, setTheme: onThemeChange}}>
        <ThemeProvider theme={themeState}>
            {props.children}
        </ThemeProvider>
    </ThemeContext.Provider>)
}

export default ThemeContextProvider;
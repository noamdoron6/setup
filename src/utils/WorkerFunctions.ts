const scriptifyWorkerObject = (workerObject: object) => {
    // remove wrapping function;
    const workerString = workerObject.toString().replace(/^\(\) \=\> \{?|\}$/g, '');
    const scriptBlob = new Blob([workerString]);
    const blobURL = window.URL.createObjectURL(scriptBlob);
    return new Worker(blobURL);
}

export default scriptifyWorkerObject;
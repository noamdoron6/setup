import { Route, Switch } from 'react-router-dom';
import { RoutesConfig } from '../../config';
import { IconicRouteType } from '../../types';
import {AppMenu} from '../common';

/*
* This component is the core of the web app, it is responsible for the routing
* between pages thus it wraps the pages with a switch and a general menu.
* (a.k.a 'AppMenu')
*/
function App() {
    const routes: IconicRouteType[] = RoutesConfig;
    return (
        <div>
            <AppMenu/>
            <Switch>
                {
                    routes.map(route => <Route key={route.path} exact path={route.path} component={route.destination}></Route>)
                }
            </Switch>
        </div>
    );
}

export default App;
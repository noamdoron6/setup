import {AppTheme} from '../theme';
import { AppLayout } from '../layout';
import { WorkersConfig } from '../../config';
import { useState } from 'react';

/*
    This page sends and receives a request from a remote server via
    a webworker.
    For this example it plays a game of 'marco polo'.
*/
function BackendTestPage() {
    const [answer, setAnswer] = useState('');
    const call = 'marco';
    const request = `${process.env.REACT_APP_BACKEND}?call=${call}`;
    let httpWorker = WorkersConfig.find(workerEntry => workerEntry.name === 'http')?.worker;
    if(httpWorker !== undefined) {
        httpWorker.onmessage = (event) => {
            setAnswer(event.data);
        }
        httpWorker.postMessage(request);
    }

    return (
        <AppTheme>
            <AppLayout>
                call: `{call}!!`
                <br/>
                response: `{answer}`
            </AppLayout>
        </AppTheme>
    );
}

export default BackendTestPage;

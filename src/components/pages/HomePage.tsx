import logo from '../../assets/logo.svg';
import {AppTheme} from '../theme';
import { AppLayout } from '../layout';
import { SpinningLogo } from '../common';

function HomePage() {
    return (
        <AppTheme>
            <AppLayout>
                <SpinningLogo src={logo} height={20} interval={10}></SpinningLogo>
                <p>
                    Home Page
                </p>
            </AppLayout>
        </AppTheme>
    );
}

export default HomePage;

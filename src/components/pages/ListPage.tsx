import {AppTheme} from '../theme';
import { AppLayout } from '../layout';
import styled from 'styled-components';
import { SyntheticEvent, useState } from 'react';
import { Buttons } from '../common';

const ListItem = styled.p`${props => `
    color: ${props.theme.primary};
    margin: 0.5em;
    display: inline-block;
`}`;
const ListIn = styled.input`${props => `
    font-size: 1em;
    background-color: inherit;
    border: none;
    border-bottom: 2px solid ${props.theme.primary};
    color: ${props.theme.primary};
    &:focus {
        outline: none;
    }
    &::placeholder {
        color: ${props.theme.primary};
        opacity: 0.8;
    }
`}`;

function ListPage() {
    const [items, setItems] = useState(['item 1', 'item 2', 'item 3', 'item 4']);
    const onKeyDown = (keyEvent: SyntheticEvent ) => {
        let input = (keyEvent.target as HTMLInputElement).value;
        let key = (keyEvent as unknown as {key: string}).key;
        if(key === "Enter" && input.length > 0) {
            let newItems = items.concat([input]);
            setItems(newItems);
            (keyEvent.target as HTMLInputElement).value = '';
        }
    };
    const deleteItem = (index: number) => {
        let newItems = items.concat([]);
        newItems.splice(index, 1);
        setItems(newItems);
    };

    return (
        <AppTheme>
            <AppLayout>
                <ListIn placeholder="enter item here..." onKeyDown={onKeyDown}/>
                {
                    items.map((item, index) => 
                    <div key={index}>
                        <ListItem>{item}</ListItem>
                        <Buttons.DeleteButton onClick={() => deleteItem(index)}/>
                    </div>)
                }
            </AppLayout>
        </AppTheme>
    );
}

export default ListPage;

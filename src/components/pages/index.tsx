import HomePage from './HomePage';
import ListPage from './ListPage';
import BackendTestPage from './BackendTestPage';

/*
* This is an exportation file, it allows easier exportation of objects,
* components, types and globals from this directory.
*/
export {
    HomePage,
    ListPage,
    BackendTestPage
};
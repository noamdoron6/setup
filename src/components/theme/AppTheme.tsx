import styled from 'styled-components';
import backround from '../../assets/desert.jpeg';

/*
* This is Theming component used to standardize the theme of the app.
* (this includes colors, fonts and backgrounds)
*/
const AppTheme = styled.div`${props => `
    background-color: ${props.theme.background};
    background-image: url(${backround});
    background-size: cover;
    text-align: center;
    font-size: calc(10px + 2vmin);
    color: ${props.theme.textPrimary};
`}`;

export default AppTheme;
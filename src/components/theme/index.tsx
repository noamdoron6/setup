import AppTheme from './AppTheme';

/*
* This is an exportation file, it allows easier exportation of objects,
* components, types and globals from this directory.
*/
export {
    AppTheme
}
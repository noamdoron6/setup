import styled from 'styled-components';

/*
* This layout determines the general positioning of the content of a page.
*/
const AppLayout = styled.div`
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export default AppLayout;
import AppLayout from './AppLayout';

/*
* This is an exportation file, it allows easier exportation of objects,
* components, types and globals from this directory.
*/
export {AppLayout};
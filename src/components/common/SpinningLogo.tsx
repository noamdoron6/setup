import React from 'react';
import styled, { keyframes } from 'styled-components';

const SpinClockwiseAnimation = keyframes`
    from {
        transform: rotate(0deg);
    }
    to {
        transform: rotate(360deg);
    }
`;

const SpinAntiClockwiseAnimation = keyframes`
    from {
        transform: rotate(360deg);
    }
    to {
        transform: rotate(0deg);
    }
`;

const Logo = styled.img<{height: number, interval: number, clockwise: boolean}>`
    height: ${props => props.height}vmin; // 40vmin;
    pointer-events: none;
    animation: ${props => props.clockwise ? SpinClockwiseAnimation : SpinAntiClockwiseAnimation} infinite ${props => props.interval}s linear;
`;

export interface SpinningLogoProps {
  src: string,
  
}

const SpinningLogo = (props: {src: string, height: number, interval: number, clockwise?: boolean}) => {
    let direction = props.clockwise ?? false;
    return (<Logo height={props.height} src={props.src} interval={props.interval} clockwise={direction}/>);
};

export default React.memo(SpinningLogo);
import { Buttons } from '.';
import styled from 'styled-components';
import { useContext, useRef } from 'react';
import { ThemeContext } from '../../context';

const MenuTab = styled.div`${props => `
    min-height: 1.5em;
    min-width: 2em;
    border-radius: 1em;
    transition: all 1000ms;
    background-color: ${props.theme.accent};
    box-shadow: 0.5em 0.5em 1em rgb(0, 0, 0, 50%);
`}`;
const ButtonsBar = styled.div`${props => ``}`;
const ChildrenBar = styled.div`${props => `
    visibility: hidden;
    transform: scaleY(0);
    max-height: 0;

    &.active {
        visibility: visible;
        max-height: none;
        transform: scaleY(1);
        padding: 0 1em 0.5em 1em;
    }
`}`;

/*
    An expandable menu that holds a button-header section and children section,
    using the "burger button" for expanding the menu.
    (the buttons are made for quick actions while the children are actions that
        should be accessed but not as frequent)
*/
const BurgerMenu = (props: {children?: any, buttons?: any}) => {
    const themeContext = useContext(ThemeContext);
    const childrenBarRef = useRef(null);
    const toggle = () => {
        if (childrenBarRef !== null) {
            const childrenBar = childrenBarRef.current as unknown as HTMLElement;
            childrenBar.classList.toggle('active');
        }
    }

    return (<MenuTab>
        <ButtonsBar>
            <Buttons.BurgerButton onClick={toggle} color={themeContext.theme.secondary}/>
            {props.buttons}
        </ButtonsBar>
        <ChildrenBar ref={childrenBarRef}>
            {props.children}
        </ChildrenBar>
    </MenuTab>)
}

export default BurgerMenu;
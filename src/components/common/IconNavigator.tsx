import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { ThemeContext } from '../../context';
import { IconicRouteType } from '../../types';
import {Buttons} from '../common';

/*
* A navigator component based on animated icon components as its navigation links.
*/
const IconNavigator = (props: {routes: IconicRouteType[]}) => {
    let history = useHistory();
    const themeContext = useContext(ThemeContext);

    return(<>
        {
            props.routes.map((route, index) => <route.icon key={index} color={themeContext.theme.secondary} onClick={() => history.push(route.path)}/>)
        }
    </>);
}

export default IconNavigator;
import styled from 'styled-components';
import { BurgerMenu, IconNavigator } from '.';
import { RoutesConfig } from '../../config';

const StyledAppMenu = styled.div`
    position: fixed;
    top: 1em;
    left: 1em;
`;

/*
* This component serves as the main menu of the app, it is responsible for
* the app nevagation links as well other actions.
*/
const AppMenu = () => {
    const routes = RoutesConfig;

    return (
        <StyledAppMenu>
            <BurgerMenu buttons={<>
                <IconNavigator routes={routes}/>
            </>}>
            <p>
                Hello world, 
            </p>
            <p>Lorem ipsum dolor sit amet</p>
            <p>consectetur adipiscing elit</p>
            </BurgerMenu>
        </StyledAppMenu>
    );
};

export default AppMenu;
import styled from 'styled-components';

/*
    This 'container' is responsibe for the graphic behaviours
    of the button.
*/
const GarbageBinContainer = styled.svg<{color?: string}>`${props => `
    // base button
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;

    // paths properties
    .line {
        fill: ${props.color ?? props.theme.primary};
        transition: stroke-dasharray 400ms, visibility 400ms;
        stroke: ${props.color ?? props.theme.primary};
        stroke-width:0.5;
        stroke-linecap:round;
        filter: brightness(20%);
    }

    .lid {
        transition: 0.25s;
    }
    .handle {
        transition: 0.25s;
    }

    :hover {
        .lid {
            transform: translate(5px, -5px) rotate(5deg);
        }
        .handle {
            transform: translate(5px, -5px) rotate(5deg);
        }
    }
`}`;

/*
    This button is shaped like a garbage bin with lid opening animation,
    it symbolizes deletion of an element from the app.
*/
const DeleteButton = (props: {onClick?: () => void, color?: string}) => {
    const toggle = () => {
        props.onClick?.();
    }
    
    return (<>
       <GarbageBinContainer viewBox="35 15 20 30" width="30" onClick={toggle} color={props.color}>
           <path className={'line handle'} d="m 40.66935,23.188565 0.03004,-2.378034 c 0.363366,-1.040621 0.834747,-1.360922 1.317543,-1.605104 1.013062,-0.09016 2.76356,-0.0407 3.458631,0.05859 0.655771,0.22878 1.085497,0.808136 1.345822,1.650222 l -0.03004,2.378034 c -0.406098,0.279747 -0.849064,0.393149 -1.409608,-0.02388 l -0.01836,-1.969544 c -0.07067,-0.178357 -0.08269,-0.389032 -0.427158,-0.416546 l -2.356194,-0.03991 c -0.288964,0.104568 -0.380042,0.282028 -0.438378,0.471549 l -0.06722,1.898427 c -0.497267,0.384752 -0.962479,0.334105 -1.40507,-0.0238 z" />
           <path className={'line lid'} d="m 40.04315,19.900381 c -0.131918,0.514847 -0.331024,0.968822 -0.306577,1.625337 -6.644195,1.989227 -0.82122,5.167072 4.005482,5.061391 4.434991,-0.0971 10.896054,-2.210368 4.10806,-4.941519 -0.02799,-0.554405 -0.07071,-1.10492 -0.279966,-1.611464 3.335741,0.870032 4.413922,2.215261 4.592249,3.973102 0.760616,7.497705 -17.089421,7.225097 -16.915307,0.17968 0.04743,-1.919343 1.487795,-3.666278 4.796059,-4.286527 z" />
           <path className={'line bin'} d="m 36.231327,26.929939 1.080937,13.268347 c 0.620716,1.705039 3.322934,2.932782 6.30977,2.895942 2.387864,-0.02945 5.262936,-0.609556 6.445301,-2.895942 l 1.080938,-13.268347 c -0.448034,0.417322 -0.789561,-0.03593 -0.93974,0.827221 -3.108124,2.187452 -9.203397,2.361345 -12.888802,0.01004 -1.092779,-4.272387 5.465306,0.114221 5.359244,-0.09728 -0.147308,-0.293741 -6.471512,-4.180907 -6.447648,-0.739983 z" />
        </GarbageBinContainer> 
    </>);
}

export default DeleteButton;
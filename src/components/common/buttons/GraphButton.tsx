import styled from 'styled-components';

/*
    This 'container' is responsibe for the graphic behaviours
    of the button.
*/
const GarbageBinContainer = styled.svg<{color?: string}>`${props => `
    // base button
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;

    // paths properties
    .line {
        stroke: ${props.color ?? props.theme.primary};
        stroke-width: 0.75;
        stroke-linecap:round;
        fill:none
    }

    .arch1 {
        fill: ${props.color ?? props.theme.primary};
        transition: 500ms;
        opacity: 1;
        transform: translate(-1px, 1px);
    }
    .arch2 {
        fill: ${props.color ?? props.theme.primary};
        transition: 500ms;
        opacity: 0.6;
        transform: translate(1px, -1px);
    }
    .arch3 {
        fill: ${props.color ?? props.theme.primary};
        transition: 500ms;
        opacity: 0.3;
        transform: translate(-2px, -1px);
    }

    .handle {
        visibility: hidden;
        transition: 500ms;
        stroke-dasharray: 0 100;
    }

    :hover {
        .arch1 {
            transform: translate(0, 2px);
        }
        .arch2 {
            transform: translate(2px, 0);
        }
        .arch3 {
            transform: translate(0, -2px);
        }
        .handle {
            visibility: visible;
            stroke-dasharray: 40 0;
        }
    }
`}`;

/*
    This button is shaped like a garbage bin with lid opening animation,
    it symbolizes deletion of an element from the app.
*/
const GraphButton = (props: {onClick?: () => void, color?: string}) => {
    const toggle = () => {
        props.onClick?.();
    }
    
    return (<>
       <GarbageBinContainer viewBox="0 -20 120 120" width="80" onClick={toggle} color={props.color}>
           <path className={'line arch1'} d="M 82.598255,51.496479 A 29.543602,27.573242 0 0 1 61.092419,78.029631 29.543602,27.573242 0 0 1 27.884633,65.933937 29.543602,27.573242 0 0 1 31.321149,32.819167 l 21.733504,18.677312 z" /> 
           <path className={'line arch2'} d="M 78.383521,22.449818 A 37.044189,39.128391 0 0 1 90.298227,51.100292 L 53.254154,51.19854 Z" />       
           <path className={'line arch3'} d="M 28.503926,29.753841 A 32.949089,31.811857 0 0 1 74.100758,26.659885 L 52.966278,51.065399 Z" />       
           <path className={'line handle'} d="m 82.710511,68.795567 c 0.196027,-2.978534 -3.386023,-2.512178 -3.598571,-0.58598 -0.09696,0.878098 1.274118,3.055779 3.274946,1.366322 l 12.805884,6.857976 -2.121223,11.296361 v 8.9221 H 37.976929 v -8.9221 h 52.182378" />
           <path className={'line handle'} d="m 90.832442,30.665156 c 0.13465,2.0859 -2.325655,1.759309 -2.471642,0.410364 -0.0666,-0.614936 0.875099,-2.139998 2.249365,-0.956852 L 105.88574,22.849707 100.64741,5.2454711 V -1.0027928 H 62.806219 v 6.2482639 h 35.840953" />
           <path className={'line handle'} d="m 22.819331,24.267155 c -0.117713,1.85389 2.032877,1.563626 2.160489,0.36472 0.05818,-0.54654 -0.76494,-1.90197 -1.966187,-0.850426 L 8.9462268,16.376852 14.239977,1.6748168 V -3.8784731 H 47.317305 V 1.6748168 H 15.988406" />
        </GarbageBinContainer> 
    </>);
}

/*
<path className={'line handle'} d="m 89.861341,31.52707 c 0.0841,1.227328 -1.45256,1.035164 -1.54374,0.241455 -0.0416,-0.361824 0.54657,-1.259159 1.40491,-0.563005 l 9.54083,-4.277003 -3.27176,-10.358205 V 12.893879 H 72.356702 v 3.676433 h 22.385569" />
           <path className={'line handle'} d="m 29.937231,24.840324 c -0.08411,1.227327 1.45256,1.035164 1.543743,0.241455 0.04157,-0.361825 -0.546576,-1.259158 -1.404908,-0.563006 l -10.051643,-4.90205 3.782566,-9.7331577 v -3.676433 h 23.634882 v 3.676433 h -22.38557" />
*/

export default GraphButton;
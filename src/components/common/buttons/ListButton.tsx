import styled from 'styled-components';

/*
    This 'container' is responsibe for the graphic behaviours
    of the button.
*/
const ListContainer = styled.svg<{color?: string}>`${props => `
    // base button
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;

    // paths properties
    .line {
        fill:none;
        transition: stroke-dasharray 400ms, visibility 400ms;
        stroke: ${props.color ?? props.theme.accent};
        stroke-width:3.5;
        stroke-linecap:round;
    }
    .check {
        visibility: hidden;
        stroke-dasharray: 0 100;
    }
    :hover {
        .check {
            visibility: visible;
            stroke-dasharray: 40 0;
        }
    }
`}`;

/*
    This button is shaped like checklist, and animates to fill its marks.
*/
const ListButton = (props: {onClick?: () => void, color?: string}) => {
    const toggle = () => {
        props.onClick?.();
    }
    
    return (<>
       <ListContainer viewBox="0 0 100 100" width="80" onClick={toggle} color={props.color}>
           <path className={'line box'} d="m 10.186171,25.186171 c 0,0 0,0 13.591264,0 0,12.495744 0,12.495744 0,12.495744 H 10.186171 Z" />
           <path className={'line box'} d="m 10.186171,41.252127 c 0,0 0,0 13.591266,0 0,12.495746 0,12.495746 0,12.495746 H 10.186171 Z" />
           <path className={'line box'} d="m 10.186171,57.318083 c 0,0 0,0 13.591266,0 0,12.495746 0,12.495746 0,12.495746 H 10.186171 Z" />
           <path className={'line underline'} d="M 27.469872,37.366711 76,37.14637" />
           <path className={'line underline'} d="M 27.469872,53.432668 70,53.212326" />
           <path className={'line underline'} d="M 27.469872,69.498628 76,69.278282" />
           <path className={'line check'} d="m 12.516101,30.879112 3.135996,3.794248 5.612778,-7.345062" />
           <path className={'line check'} d="m 12.516101,46.94507 3.135996,3.794247 5.612778,-7.345061" />
           <path className={'line check'} d="m 12.516101,63.01103 3.135996,3.794243 5.612778,-7.345061" />
        </ListContainer> 
    </>);
}

export default ListButton;
import styled from 'styled-components';

/*
    This 'container' is responsibe for the graphic behaviours
    of the button.
*/
const BackendContainer = styled.svg<{color?: string}>`${props => `
    // base button
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;

    // paths properties
    .line {
        fill: ${props.color ?? props.theme.accent};
        transition: stroke-dasharray 400ms, visibility 400ms;
        stroke: ${props.color ?? props.theme.accent};
        stroke-width:0.5;
        stroke-linecap:round;
    }

    .head {
        transition: 0.5s;
    }
    .tail {
        transition: 0.5s;
    }

    :hover {
        .head {
            transform: translate(-20px, -5px);
        }
        .tail {
            transform: translate(+20px, +5px);
        }
    }
`}`;


/*
    This button is shaped like a DB icon with expanding animation,
    it symbolizes a form of connection with the backend or DB used by
    the app.
*/
const BackendButton = (props: {onClick?: () => void, color?: string}) => {
    const toggle = () => {
        props.onClick?.();
    }
    
    return (<>
       <BackendContainer viewBox="0 0 100 100" width="80" onClick={toggle} color={props.color}>
           <path className={'line head'} d="m 23.5,23.230524 c 16.796262,-10.911218 32.9735,-8.358143 46.49979,0 v 14.114022 c -15.996285,9.825945 -31.76361,8.455278 -46.49979,0 z" />
           <path className={'line body'} d="m 23.500214,43.94323 v 14.475919 c 11.783921,9.613662 36.559202,8.465367 46.499786,0 V 43.94323 c -18.52048,12.547466 -36.704976,6.662934 -46.499786,0 z" />
           <path className={'line tail'} d="m 23.5,65.210694 v 14.475921 c 11.783919,9.613662 36.559201,8.465366 46.49979,0 V 65.210694 c -18.520485,12.547467 -36.704982,6.662934 -46.49979,0 z" />
        </BackendContainer> 
    </>);
}

export default BackendButton;
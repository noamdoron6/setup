import styled from 'styled-components';

/*
    This 'container' is responsibe for the graphic behaviours
    of the button.
*/
const HomeContainer = styled.svg<{color?: string}>`${props => `
    // base button
    cursor: pointer;
    -webkit-tap-highlight-color: transparent;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    user-select: none;

    // paths properties
    .line {
        fill:none;
        transition: stroke-dasharray 400ms, visibility 400ms;
        stroke: ${props.color ?? props.theme.accent};
        stroke-width:5.5;
        stroke-linecap:round;
    }

    .smoke {
        visibility: hidden;
        stroke-dasharray: 0 100;
    }
    .door {
        visibility: hidden;
        stroke-dasharray: 0 100;
    }
    :hover {
        .smoke {
            visibility: visible;
            stroke-dasharray: 40 0;
        }
        .door {
            visibility: visible;
            stroke-dasharray: 40 0;
        }
    }
`}`;

/*
    This button is shaped like a house with a door and chimney smoke
    animating, it symbolizes the 'home page', the root of the app.
*/
const HomeButton = (props: {onClick?: () => void, color?: string}) => {
    const toggle = () => {
        props.onClick?.();
    }
    
    return (<>
       <HomeContainer viewBox="0 0 100 100" width="80" onClick={toggle} color={props.color}>
           <path className={'line'} d="M 37.669763,69.455621 H 26.624027 V 43.199983 l 22.091476,-17.503762 11.045734,8.751894 v -8.751894 h 8.2843 v 15.315806 l 2.761427,2.187956 V 69.455621 H 59.761237 V 51.951876 H 37.669763 Z" />
           <path className={'line smoke'} d="m 61.764795,21.853344 c 3.392852,0.124468 5.701044,-2.00011 1.708057,-4.428389 -1.488389,-0.905143 -2.456293,-4.791947 2.617196,-5.013163" />
           <path className={'line door'} d="M 49.985924,69.901834 H 43.206141 V 57.370613 c 0.943609,0.003 9.832415,0.0058 10.776023,0.0087 v 12.52248" />
        </HomeContainer> 
    </>);
}

export default HomeButton;
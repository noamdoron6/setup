import scriptifyWorkerObject from "../utils/WorkerFunctions";

const HttpWorkerObject = () => {
    // eslint-disable-next-line no-restricted-globals
    self.onmessage = (event) => {
        const headers = {
            "Access-Control-Allow-Origin": "*"
        };
        fetch(event.data, {method: 'GET', headers}).then((response) => {
            // eslint-disable-next-line no-restricted-globals
            response.json().then((data) => (self as any).postMessage(data));
        });
    };
}

const HttpWorker = scriptifyWorkerObject(HttpWorkerObject);

export default HttpWorker;
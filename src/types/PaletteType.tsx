export default interface PaletteType {
    name: string,
    primary: string; // appear over background.
    secondary: string; // appear over primary/accent.
    accent: string; // actions and buttons.
    textPrimary: string; // appear over background.
    textSecondary: string; // appear over primary/secondary/accent.
    background: string;
}
export default interface WorkerEntryType {
    name: string;
    worker: Worker;
}
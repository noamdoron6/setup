interface RouteType {
    path: string;
    destination: React.ComponentType;
}

interface IconicRouteType extends RouteType {
    icon: React.ComponentType<any>;
}

export type {
    RouteType,
    IconicRouteType
}
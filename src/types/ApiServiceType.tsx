export default interface ApiServiceType {
    name: string;
    url: string;
    description: string;
}